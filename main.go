package main

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"runtime"
	"syscall"
	"time"

	"github.com/rafacas/sysstats"
)

func main() {
	var app = flag.String("app", `./hello.sh`, "Path to running application")
	var cStart = flag.Float64("cpuStart", 0.2, "% CPU load to start")
	var cLimit = flag.Float64("cpuExit", 0.8, "% CPU load to exit")
	flag.Parse()

	border := borderParam(*cStart)
	borderLimit := borderParam(*cLimit)
	log.Printf("Started limit is %.2f\n", border)

	for {
		time.Sleep(10 * time.Second)
		la := loadAverage()
		log.Printf("Border limit is %.2f\n", border)
		log.Printf("Load average are: %.2f, %.2f, %.2f", la.Avg1, la.Avg5, la.Avg15)
		var ok, started bool

		cmd := startProcess(*app)

		if border > la.Avg1 {
			err := cmd.Start()
			if err != nil {
				log.Fatalln(err)
			}
			started = true
			log.Printf("Started process from %s, PID=%d", *app, cmd.Process.Pid)
		} else {
			log.Println("No, It is full")
			time.Sleep(30 * time.Second) // set long time
		}

		ok = true
		for ok && started {
			time.Sleep(10 * time.Second)
			la = loadAverage()
			log.Printf("Load average are: %.2f, %.2f, %.2f", la.Avg1, la.Avg5, la.Avg15)
			log.Printf("Critical limit is %.2f\n", borderLimit)
			if la.Avg1 > borderLimit {
				log.Printf("Trying to kill process from %s, PID=%d", *app, cmd.Process.Pid)
				// if err := cmd.Process.Kill(); err != nil {
				if err := cmd.Process.Signal(syscall.SIGTERM); err != nil {
					log.Fatal(err)
				}
				// foundedProcess, err := os.FindProcess(cmd.Process.Pid)
				// if err != nil {
				// 	log.Fatal(err)
				// }
				// if err := foundedProcess.Kill(); err != nil {
				// 	log.Fatal(err)
				// }
				time.Sleep(10 * time.Second) // set long time
				// zombieKiller(cmd.Process.Pid)
				childProcessKiller(*cmd)
				ok = false
				started = false
				time.Sleep(30 * time.Second) // set long time
			}
		}
	}
}

func borderParam(c float64) float64 {
	cpu := runtime.NumCPU()
	return float64(cpu) * c
}

func loadAverage() *sysstats.LoadAvg {
	la, err := sysstats.GetLoadAvg()
	if err != nil {
		log.Fatalln(err)
	}
	return &la
}

func startProcess(filePath string) *exec.Cmd {
	path, err := exec.LookPath(filePath)
	if err != nil {
		log.Fatalln("File not found\n", err)
	}
	cmd := exec.Command(path)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmd.Stdout = os.Stdout
	return cmd
}

func zombieKiller(pid int) {
	// send wait() to zombie (check status)
	var waitStatus syscall.WaitStatus
	if _, err := syscall.Wait4(pid, &waitStatus, 0, nil); err != nil {
		panic(err)
	}
	log.Println("Zombie has killed, has got status", waitStatus)
	if waitStatus.Exited() {
		return
	}
}

func childProcessKiller(cmd exec.Cmd) {
	pgid, err := syscall.Getpgid(cmd.Process.Pid)
	if err == nil {
		syscall.Kill(-pgid, 15) // note the minus sign
	}
	cmd.Wait()
}
