# Wrapper for bash script

Description:  
It starts the script, when load average is low, and it kills the script, when load average is high.

We have `stress` in bash sample file (hello.sh), so you should install stress on aim node:
```bash
$ sudo apt update
$ sudo apt install stress
```

```bash
$ ./main -help           
Usage of ./main:
  -app string
    	Path to running application (default "./hello.sh")
  -cpuExit float
    	% CPU load to exit (default 0.8)
  -cpuStart float
    	% CPU load to start (default 0.2)
```
