module test

go 1.18

require (
	github.com/rafacas/sysstats v0.0.0-20150414182805-21d5ac1731f7
	github.com/shirou/gopsutil v3.21.11+incompatible
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/sys v0.0.0-20221010170243-090e33056c14 // indirect
)
